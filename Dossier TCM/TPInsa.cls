\NeedsTeXFormat{LaTeX2e}
 
\ProvidesClass{TPInsa}[06/10/2015, Template TP INSA v0.2] % Some credits goes to Luc Forget for this
 
% classe de base
 
\LoadClass[a4paper, 11pt]{article}
 
% extensions
 
\RequirePackage[utf8]{inputenc}
\RequirePackage{lmodern}
\RequirePackage[T1]{fontenc}
\RequirePackage[pdftex]{graphicx}
\RequirePackage[frenchb]{babel}

\RequirePackage{amsmath}
\RequirePackage{amsfonts}
\RequirePackage{amssymb}
\RequirePackage{lmodern}

\RequirePackage[svgnames]{xcolor}
\RequirePackage{titling}
 
 
\RequirePackage{hyperref}
\hypersetup{
    colorlinks=false, %set true if you want colored links
    linktoc=all,     %set to all if you want both sections and subsections linked
    linkcolor=blue,  %choose some color if you want links to stand out
}
 
% commandes personnelles
  
 
\newcommand{\langue}{\emph} % mots en langues étrangères
\newcommand{\cita}{\emph} % citation en italique
\newcommand{\nomprog}{\texttt} % nom de programme en police teletype

%----Variables------------------------------------------------------
\newcommand{\thematiere}{}
\newcommand{\matiere}[1]{\renewcommand{\thematiere}{#1}}


%sectionnement
\renewcommand\section{\@startsection {section}{1}{\z@}%
                                   {-3.5ex \@plus -1ex \@minus -.2ex}%
                                   {2.3ex \@plus.2ex}%
                                   {\sffamily\Large\bfseries}}
\renewcommand\subsection{\@startsection{subsection}{2}{\z@}%
                                     {-3.25ex\@plus -1ex \@minus -.2ex}%
                                     {1.5ex \@plus .2ex}%
                                     {\sffamily\large\bfseries}}
\renewcommand\subsubsection{\@startsection{subsubsection}{3}{\z@}%
                                     {-3.25ex\@plus -1ex \@minus -.2ex}%
                                     {1.5ex \@plus .2ex}%
                                     {\sffamily\normalsize\bfseries}}
\renewcommand\paragraph{\@startsection{paragraph}{4}{\z@}%
                                    {3.25ex \@plus1ex \@minus.2ex}%
                                    {-1em}%
                                    {\sffamily\normalsize\bfseries}}
\renewcommand\subparagraph{\@startsection{subparagraph}{5}{\parindent}%
                                       {3.25ex \@plus1ex \@minus .2ex}%
                                       {-1em}%
                                      {\sffamily\normalsize\bfseries}}
                                   
%Mise en forme du titre
\pretitle{\vspace{-3em}{\noindent\normalsize \sffamily \rule[0.25em]{0.5cm}{1pt} \scshape~\thematiere~~\leaders\hbox{\rule[0.25em]{1pt}{1pt}}\hfill\strut{}}

\vspace{0.3em}
\centering \sffamily  \bfseries \huge}
\preauthor{\begin{center}\sffamily \bfseries \color{gray}}
\predate{\begin{flushright} \sffamily }
\posttitle{\\\vspace{0.3em}\rule{\linewidth}{1pt}}
\postauthor{\end{center}}
\postdate{\end{flushright}}
